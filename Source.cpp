#include <iostream> 
using namespace std;

class kopiec {
public:
    int rozmiar_tablicy;
    int rozmiar_kopca;
    int arr[1000];
    kopiec(int tab[], int size) {
        rozmiar_kopca = size;
        rozmiar_tablicy = size;
        for (int i = rozmiar_kopca - 1; i >= 0; i--) arr[i] = tab[i];
    }
    void printArray() {
        for (int i = 0; i < rozmiar_tablicy; ++i) cout << arr[i] << " ";
        cout << endl;
    }
    void heapify(int i) {
        int najwiekszy;
        int lewy_i = 2 * i + 1;
        int prawy_i = 2 * i + 2;
        if (lewy_i < rozmiar_kopca && arr[lewy_i] > arr[i]) najwiekszy = lewy_i;
        else najwiekszy = i;
        if (prawy_i < rozmiar_kopca && arr[prawy_i] > arr[najwiekszy]) najwiekszy = prawy_i;
        if (najwiekszy != i) {
            swap(arr[i], arr[najwiekszy]);
            heapify(najwiekszy);
        }
    }

    void heapify_bottom_up(int i){
        int parent = (i - 1) / 2;
        if (arr[parent] > 0) {
            if (arr[i] > arr[parent]) {
                swap(arr[i], arr[parent]);
                heapify_bottom_up(parent);
            }
        }
    }

    void build_heap() {
        for (int i = rozmiar_tablicy / 2 - 1; i >= 0; i--) heapify(i);
    }

    void heapSort() {
        build_heap();
        for (int i = rozmiar_kopca - 1; i > 0; i--) {
            swap(arr[0], arr[i]);
            rozmiar_kopca--;
            heapify(0);
        }
    }

    void insert(int x) {
        arr[rozmiar_tablicy] = x;
        rozmiar_kopca++;
        rozmiar_tablicy++;
        heapify_bottom_up(rozmiar_kopca - 1);
    }

    int extract_max(){
        int max = arr[0];
        arr[0] = arr[rozmiar_kopca - 1];
        rozmiar_kopca--;
        rozmiar_tablicy--;
        heapify(0);
        return max;
    }
    
};


int main()
{
    int arr_temp[] = { 5,8,3,4,7,1 };
    kopiec xd(arr_temp, sizeof(arr_temp) / sizeof(arr_temp[0]));
    xd.build_heap();
    xd.printArray();
    cout << endl << "Dodaje 9: " << endl;
    xd.insert(9);
    xd.printArray();
    cout << endl << "Dodaje 2: " << endl;
    xd.insert(2);
    xd.printArray();
    cout << endl << "Dodaje 6: " << endl;
    xd.insert(6);
    xd.printArray();
    cout << endl;
    cout << "Usuwam najwiekszy - " << xd.extract_max() << endl << endl;
    xd.printArray();
    cout << endl << "Posortowane: " << endl;
    xd.heapSort();
    xd.printArray();
}